# La fête sauvage de Maïa 

## Date : 25 mai 2024

## Heure 15h-18h

## Lieu : Champ des possibles

<div class="responsive-iframe">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d718.7870434386267!2d-73.59801557720985!3d45.5285804338174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc9197b5c0b5079%3A0x9993cbfb63785799!2sLe%20Champ%20des%20possibles!5e1!3m2!1sen!2sca!4v1715533452018!5m2!1sen!2sca" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

</div>

![](./media/position.jpg)


## RSVP 

<a href="mailto:maiatremafiocca@gmail.com?subject=RSVP%20fete%20sauvage">
    RSVP -> maiatremafiocca@gmail.com
</a>

## Infos/contacts

## Apportez votre gourde d'eau!


### Viva 
* viva.paci@gmail.com
### Guillaume 
* guillaume.arseneault@gmail.com 
* cell 514 927 4742


<!-- 
## Code QR


![](./media/maia-fete-8-ans-bw.png)
![](./media/maia-fete-8-ans.png) -->